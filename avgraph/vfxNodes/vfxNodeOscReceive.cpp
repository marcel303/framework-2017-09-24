/*
	Copyright (C) 2017 Marcel Smit
	marcel303@gmail.com
	https://www.facebook.com/marcel.smit981

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
*/

#include "framework.h"
#include "vfxNodeOscReceive.h"

VFX_NODE_TYPE(osc_receive, VfxNodeOscReceive)
{
	typeName = "osc.receive";
	
	in("port", "int");
	in("ipAddress", "string");
	in("reset!", "trigger");
	out("event", "trigger");
	out("channels", "channels");
}

VfxNodeOscReceive::VfxNodeOscReceive()
	: VfxNodeBase()
	, OscReceiveHandler()
	, oscReceiver()
	, addressToChannelIndex()
	, channelNames()
	, channelData()
	, channelsOutput()
	, history()
{
	resizeSockets(kInput_COUNT, kOutput_COUNT);
	addInput(kInput_Port, kVfxPlugType_Int);
	addInput(kInput_IpAddress, kVfxPlugType_String);
	addInput(kInput_Reset, kVfxPlugType_Trigger);
	addOutput(kOutput_Trigger, kVfxPlugType_Trigger, nullptr);
	addOutput(kOutput_Channels, kVfxPlugType_Channels, &channelsOutput);
}

VfxNodeOscReceive::~VfxNodeOscReceive()
{
	oscReceiver.shut();
}

void VfxNodeOscReceive::init(const GraphNode & node)
{
	const char * ipAddress = getInputString(kInput_IpAddress, "");
	const int udpPort = getInputInt(kInput_Port, 0);
	
	oscReceiver.init(ipAddress, udpPort);
}

void VfxNodeOscReceive::tick(const float dt)
{
	vfxCpuTimingBlock(VfxNodeOscReceive);
	
	const char * ipAddress = getInputString(kInput_IpAddress, "");
	const int udpPort = getInputInt(kInput_Port, 0);
	
	if (oscReceiver.isAddressChange(ipAddress, udpPort))
	{
		oscReceiver.shut();
		
		oscReceiver.init(ipAddress, udpPort);
	}
	
	oscReceiver.tick(this);
}

void VfxNodeOscReceive::handleTrigger(const int inputSocketIndex)
{
	if (inputSocketIndex == kInput_Reset)
	{
		addressToChannelIndex.clear();
		channelNames.clear();
		channelData.clear();
		channelsOutput.reset();
	}
}

void VfxNodeOscReceive::getDescription(VfxNodeDescription & d)
{
	const char * ipAddress = getInputString(kInput_IpAddress, "");
	const int udpPort = getInputInt(kInput_Port, 0);
	
	d.add("target: %s:%d", ipAddress, udpPort);
	d.newline();
	
	d.add("channels:");
	for (int i = 0; i < channelNames.size(); ++i)
		d.add("%02d: %s", i, channelNames[i].c_str());
	d.newline();
	
	d.add("received messages:");
	for (auto & h : history)
		d.add("%s", h.addressPattern.c_str());
}

void VfxNodeOscReceive::handleOscMessage(const osc::ReceivedMessage & m, const IpEndpointName & remoteEndpoint)
{
	const char * addressPattern = nullptr;
	float value = 0.f;
	
	try
	{
		addressPattern = m.AddressPattern();
	}
	catch (std::exception & e)
	{
		logError("failed to handle OSC message: %s", e.what());
		
		return;
	}
	
	// todo : store OSC values in trigger mem
	
	auto indexItr = addressToChannelIndex.find(addressPattern);
	
	if (indexItr == addressToChannelIndex.end())
	{
		Assert(channelsOutput.size == addressToChannelIndex.size());
		
		const int index = addressToChannelIndex.size();
		indexItr = addressToChannelIndex.insert(std::make_pair(addressPattern, index)).first;
		channelNames.push_back(addressPattern);
		
		channelData.resize(channelData.size() + 1);
		channelsOutput.setDataContiguous(&channelData.front(), false, 1, channelData.size());
	}
	
	channelData[indexItr->second] = value;
	
	trigger(kOutput_Trigger);
	
	//

	HistoryItem historyItem;
	historyItem.addressPattern = addressPattern;
	history.push_front(historyItem);
	
	while (history.size() > kMaxHistory)
		history.pop_back();
}
